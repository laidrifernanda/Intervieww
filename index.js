const express = require("express");
const app = express();
const port = 3000;

app.get("/users", (req, res) => {
  res.json([
    "Nama : Fernanda",
    "Jenis Kelamin: Laki-laki",
    "Alamat: Palembang",
  ]);
});

app.listen(port, () => {
  console.log(`this app running on ${port}`);
});
